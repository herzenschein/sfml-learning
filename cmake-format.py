with section("format"):

  line_width = 80
  tab_size = 4
  dangle_parens = True
  max_pargs_hwrap = 1
  max_prefix_chars = 1
