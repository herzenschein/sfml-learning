module;
#include <SFML/Graphics.hpp>
export module EventManager;

export class EventManager
{
  public:
    EventManager(sf::RenderWindow &window, sf::Event &event);

    void handleEvent();
    sf::RenderWindow &m_window;
    sf::Event &m_event;
    sf::Font m_font;
    sf::Text clickText;
    sf::Text connectedText;
    sf::Time timeToSleep;
};
