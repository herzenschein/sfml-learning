module;
#include <SFML/Graphics.hpp>
module EventManager;

EventManager::EventManager(sf::RenderWindow &window, sf::Event &event)
    : m_window(window)
    , m_event(event)
    , connectedText("Joystick connected!", m_font, 24)
    , clickText("Click!", m_font, 24)
    , timeToSleep(sf::seconds(0.2f))
{
    m_font.loadFromFile("undefined-medium.otf");
};

void EventManager::handleEvent()
{
    if (m_event.type == sf::Event::Closed)
    {
        m_window.close();
    }
    if (m_event.type == sf::Event::Resized)
    {
        // Boilerplate resize view upon Resized
        sf::FloatRect newWindowSize = sf::FloatRect(0.f,
                                                    0.f,
                                                    m_window.getSize().x,
                                                    m_window.getSize().y);
        sf::View viewUponResize = sf::View(newWindowSize);
        m_window.setView(viewUponResize);
        // Boilerplate end
    }
    if (m_event.type == sf ::Event::MouseButtonPressed)
    {
        clickText.setFillColor(sf::Color::Red);
        // Boilerplate center text
        clickText.setOrigin(clickText.getGlobalBounds().getSize() / 2.f
                            + clickText.getLocalBounds().getPosition());
        clickText.setPosition(m_window.getSize().x - (m_window.getSize().x / 2),
                              m_window.getSize().y - (m_window.getSize().y / 2));
        // Boilerplate end
        m_window.clear(sf::Color::White);
        m_window.draw(clickText);
        m_window.display();

        sf::sleep(timeToSleep);
        m_window.clear(sf::Color::White);
        m_window.display();
    }
    if (m_event.type == sf::Event::JoystickConnected)
    {
        connectedText.setFillColor(sf::Color::Blue);
        // Boilerplate center text
        connectedText.setOrigin(connectedText.getGlobalBounds().getSize() / 2.f
                                + connectedText.getLocalBounds().getPosition());
        connectedText.setPosition(m_window.getSize().x - (m_window.getSize().x / 2),
                                  m_window.getSize().y - (m_window.getSize().y / 2));
        // Boilerplate end
        m_window.clear(sf::Color::White);
        m_window.draw(connectedText);
        m_window.display();
    }
}
