module;
#include <SFML/Graphics.hpp>
export module Game;

export class Game
{
  public:
    explicit Game();
    void run();
    sf::RenderWindow m_window;
    sf::Event m_event;
};
