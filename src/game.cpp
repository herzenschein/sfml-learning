module;
#include <SFML/Graphics.hpp>
module Game;
import EventManager;

Game::Game()
    : m_window(sf::VideoMode(200, 200), "SFML window")
{
}

void Game::run()
{
    while (m_window.isOpen())
    {
        m_window.setVerticalSyncEnabled(true);
        while (m_window.pollEvent(this->m_event))
        {
            EventManager eventManager(m_window, m_event);
            eventManager.handleEvent();
        }
        m_window.clear(sf::Color::White);
        m_window.display();
    }
}
