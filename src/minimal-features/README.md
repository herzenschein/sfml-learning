# Minimal examples of modern C++ features

This part of the repository contains examples designed to be minimal and demonstrative.
By nature they are not supposed to care about code correctness. No const, constexpr,
`[[nodiscard]]`, access specifiers (public/private/friend), and whatnot.

This also links to good documentation about each feature. Nothing for experts, only for
beginners and intermediates. All links are structured from the easiest to understand to more complex explanations.

To build these examples, run the following from the root of the repository:

```cpp
cmake --preset default
cmake --build --preset minimal-features --parallel
```

# Print

std::print and std::println are syntactic sugar for a printable std::format.
It has Unicode support by default, replacement fields ({}), format specifiers,
and it is usually as fast as printf (or close).

* [std::print in C++23, by the creator of fmt and std::format](https://vitaut.net/posts/2023/print-in-cpp23/)
* [Format specifiers cheatsheet](https://hackingcpp.com/cpp/libs/fmt.html#quick-fmt-specs)
* [fmt explanation of format specifiers](https://fmt.dev/latest/syntax/)
* [std explanation of format specifiers](https://en.cppreference.com/w/cpp/utility/format/spec)

# Optional

std::optional is used to avoid using unique values for nullables (-1, nullptr).
It can also be used to perform lazy loading of resources, replace default parameters,
and handle the result of computations that return neither a value or an error.

Fundamentally, it should allow you to do the following:

```cpp
auto optionalResult = maybeReturnsSomething();
if (optionalResult)
    // do something
```

Accessing an invalid optional::value() throws std::bad_optional_access.

* [Using C++17 std::optional - C++ Stories](https://www.cppstories.com/2018/05/using-optional/)
* [Functional exception-less error handling with C++23 optional and expected - MS C++ Team DevBlog](https://devblogs.microsoft.com/cppblog/cpp23s-optional-and-expected/)
* [How to Deal with OPTIONAL Data in C++ - TheCherno - YouTube](https://youtu.be/UAAiwObNhQ0)
* [C++ Weekly - Ep 87 - std::optional - YouTube](https://youtu.be/PiaZkNp_fIM)
* [How to Use Monadic Operations for std::optional in C++23 - C++ Stories](https://www.cppstories.com/2023/monadic-optional-ops-cpp23/)

# Expected

std::expected is used to enforce correctness in error handling.

Fundamentally, it should *force* you to do the following:

```cpp
if (expectedResult)
    // do something with result.value()
else
    // do something with result.error()
```

This could be achieved with optional, exceptions, variant, if/else,
separate struct, output pointer to return error code,
but instead this raises the error to the type system level.

Accessing an invalid expected::value() throws std::bad_expected_access.

* [Using std::expected from C++23 - C++ Stories](https://www.cppstories.com/2024/expected-cpp23/)
* [C++23 - expected Header - GeeksforGeeks](https://www.geeksforgeeks.org/cpp-23-expected-header/)
* [C++23: A New Way of Error Handling with std::expected - ModernesC++](https://www.modernescpp.com/index.php/c23-a-new-way-of-error-handling-with-stdexpected/)
* [Using the C++23 std::expected type](https://mariusbancila.ro/blog/2022/08/17/using-the-cpp23-expected-type/)
* [C++23: The expected header; expect the unexpected | Sandor Dargo's Blog](https://www.sandordargo.com/blog/2022/11/16/cpp23-expected)
* [C++ Weekly - Ep 421 - You're Using optional, variant, pair, tuple, any, and expected Wrong! - YouTube](https://youtu.be/0yJk5yfdih0)

# Ranges

Ranges is so extremely flexible I can't describe it well here. It just makes everything in life easier.

See a few benefits of ranges I wrote on Mastodon:

* [Replacing begin() and end()](https://furry.engineer/@herzenschein/111858985917646963)
* [Functional chaining](https://furry.engineer/@herzenschein/111715450210991703)
* [Range-based operations](https://furry.engineer/@herzenschein/111610311051545519)

Now docs:

* [C++ code samples before and after Ranges - Marius Bancila blog](https://mariusbancila.ro/blog/2019/01/20/cpp-code-samples-before-and-after-ranges/)
* [The Ranges Library in C++20: Design Choices - ModernesC++ BLOG](https://www.modernescpp.com/index.php/the-ranges-library-in-c20-design-choices/)
* [Better C++ Ranges - Arno Schödl - [CppNow 2021] - YouTube](https://youtu.be/P8VdPsLLcaE)
* [An Overview of Standard Ranges - Tristan Brindle - CppCon 2019 - YouTube](https://youtu.be/SYLgG7Q5Zws)
* [C++20 Ranges in Practice - Tristan Brindle - CppCon 2020 - YouTube](https://youtu.be/d_E-VLyUnzc)

# Concepts

Concepts are used to constrain. They might as well have been called "constraints".

Fundamentally, it should allow you to do this:

```cpp
void compute(SomeConstraint auto paramOnlyAcceptedIfConstraintSatisfied)
{/* ... */}
```

To understand Concepts you first must have a good grasp of templates. You might as well learn the modern way of doing templates together with Concepts.

* [Back to Basics: Templates in C++ - Nicolai Josuttis - CppCon 2022 - YouTube](https://youtu.be/HqsEHG0QJXU)
* [Reintroduction to Generic Programming for C++ Engineers - Nick DeMarco - C++Now 2024 - YouTube](https://youtu.be/tt0-7athgrE)
* [4 ways to use C++ concepts in functions | Sandor Dargo's Blog](https://www.sandordargo.com/blog/2021/02/17/cpp-concepts-4-ways-to-use-them)
* [C++20 Concepts - a Quick Introduction - C++ Stories](https://www.cppstories.com/2021/concepts-intro/)
* [Abbreviated Function Templates and Constrained Auto - MS C++ Team DevBlog](https://devblogs.microsoft.com/cppblog/abbreviated-function-templates-and-constrained-auto/)
* [C++20 Templates: The next level: Concepts and more - Andreas Fertig - CppCon 2021 - YouTube](https://youtu.be/_FoXWnrGuNU)
* [C++ Weekly - Ep 194 - From SFINAE To Concepts With C++20 - YouTube](https://youtu.be/dR64GQb4AGo)
* [Using Modern C++ to Eliminate Virtual Functions - Jonathan Gopel - CppCon 2022 - YouTube](https://youtu.be/gTNJXVmuRRA)


# Todo / Toadd

* templates with requires
* abbreviated function templates
* SFINAE
* plain optional
* functions as parameters
* safe pointer arithmetics with std::span
