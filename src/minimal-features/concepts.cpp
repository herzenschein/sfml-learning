#include <functional>
#include <print>

using namespace std;

/* ======================================================================================== */

// Example with type trait is_convertible_v<from, to>, same as is_convertible<from, to>::value
template <class OnlyString>
concept StringLike = is_convertible_v<OnlyString, string_view>;

void printOnlyStringLikes(StringLike auto string) { println("{}", string); }

struct NonPrintableClass {};

/* ======================================================================================== */

// Example with multiple Concepts; similar to using is_arithmetic_v
// Anything after the = is just a boolean expression!
template <class OnlyNum>
concept Number = integral<OnlyNum> || floating_point<OnlyNum>;

void printOnlyIntsDoublesFloats(Number auto num) { println("{}", num); }

/* ======================================================================================== */

// Example with a custom defined Concept
template <class Bird>
concept CanFly = requires (Bird b)
{
    // Whatever is here is used to test whether the passed argument follows the CanFly constraint!
    b.fly();
};

struct Eagle {
    string name = "Eagle";
    void fly() {};
};

struct Dog {
    string name = "Dog";
    void bark() {};
};

template <class AnyAnimal>
void testFly (AnyAnimal animal)
{
    if (!CanFly<AnyAnimal>)
    {
        println("{} can't fly!", animal.name);
        return;
    };
    println("{} can fly!", animal.name);
}

/* ======================================================================================== */

int main()
{
    // Using a type trait
    printOnlyStringLikes("Mlem 😛"); // OK!
    // printOnlyStringLikes(30); // Compiler error!
    // printOnlyStringLikes('a'); // Compiler error!
    // NonPrintableClass nonPrintable;
    // printOnlyStringLikes(nonPrintable); // Compiler error!

    // Using multiple Concepts
    printOnlyIntsDoublesFloats(5); // OK!
    printOnlyIntsDoublesFloats(3.0); // OK!
    printOnlyIntsDoublesFloats(7.f); // OK!
    printOnlyIntsDoublesFloats('a'); // OK! Special case!
    // printOnlyIntsDoublesFloats("b"); // Compiler error!
    // printOnlyIntsDoublesFloats(NonPrintableClass()); // Compiler error!

    // Using a custom Concept
    testFly(Eagle()); // Passes the CanFly function constraint!
    testFly(Dog()); // Does not pass the CanFly function constraint!
}
