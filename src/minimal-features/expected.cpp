#include <expected>
#include <print>

using namespace std;

expected<double, string> divide(double numerator, double denominator)
{
    if (numerator != 0 && denominator != 0 )
        return {numerator / denominator};
    return unexpected("Cannot divide by 0!");
}

void printResult(double first, double second)
{
    auto result = divide(first, second);
    if (result)
        // :.2f is a format specifier meaning "truncate to two digits after dot"
        println("Division got result: {:.2f}", result.value());
    else println("{}", result.error());
}

int main()
{
    printResult(50, 3);  // 16.67
    printResult(621, 5); // 124.20
    printResult(4, 0);   // Cannot divide by 0!
    printResult(0, 999); // Cannot divide by 0!
}
