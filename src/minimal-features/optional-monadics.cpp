#include <optional>
#include <print>

using namespace std;

constexpr optional<int> getNum(){ return 42; };
constexpr optional<int> getZero(){ return {}; };
constexpr optional<int> byTwo(int value){ return value / 2; };

int main()
{
    /* value_or = returns the contained value if available,
     *            another value otherwise
     */

    // If maybeNum() has a value, return the value
    // else, return value 0
    optional<int> maybeNum;
    println("{}", maybeNum.value_or(0));
    // Returns 0 since maybeNum is empty



    /* or_else = returns the optional itself if it contains a value,
     *           or the result of the given function otherwise
     */

    // If getNum() has a value, return the result of getNum()
    // else, run getZero()
    println("{}", getNum().or_else(getZero).value());
    // Returns the optional itself, to print it use optional<int>::value()



    /* transform = returns an optional containing the transformed contained value
     *             if it exists,
     *             or an empty optional otherwise
     */

    // Run getNum(), then *definitely* divide it byTwo()
    // else return nullopt
    optional<int> numByTwo = getNum().transform(byTwo).value();
    println("{}", numByTwo.value() );
    // Returns 21
    // A transform returns optional<optional<int>>, so we call value() twice!



    /* and_then = returns the result of the given function on the contained value
     *            if it exists,
     *            or an empty optional otherwise
     */

    // If maybeNum() has a value, divide byTwo()
    // else, return nullopt
    optional<int> numByTwoElseEmpty = maybeNum.and_then(byTwo);
    if (!numByTwoElseEmpty) println("maybeNum is empty");
    // Prints "maybeNum is empty" because nullopt was returned
}
