#include <print>
#include <format>
#include <iostream>

int main()
{
    using namespace std;
    println("Print is {}, but...", "cool.");
    string deps = "cpp14 gcc14 gcc14-c++ libstdc++6-devel-gcc14";
    println("I needed to install {} for it to even work, even for clang for some reason.", deps);
    println();
    print("Anyways, you can do {} placeholders with {} and {} ", "multiple placeholders", "print", "println");
    println("and even {1} since print is based on {0}, but you have to remember that it starts from {2}.", "format", "numbered placeholders", 0);
    print(stderr, "You can output to a stream or file too, ");
    cout << format("and unlike format, you don't need cout or \\n.\n");
    println("In fact, print is just syntactic sugar for format with guaranteed Unicode support 🐰.");
    double pi = 3.1415926;
    println("It even has cool things like format specifiers. See how pi gets reduced to only three digits after the dot: {:.3f}", pi);
}
