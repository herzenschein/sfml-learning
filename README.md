# Bleeding edge SFML Project with modern C++ / CMake

![pipeline](https://ci.codeberg.org/api/badges/13741/status.svg)

Just a project I'm using to learn SFML (with a few extras).

The code is GPLv3 (the better GPL since it has the tivoization clause 🔥),
and the font is
[undefined-medium](https://github.com/andirueckel/undefined-medium),
which is under the Open Font License.

This project shows a few cool things:

* How to use SFML with CMake (simple, but why doesn't upstream have these instructions...).
* How to use C++20 modules with split implementation and header/interface files.
* How to split the SFML event loop from main.
* Minimal examples of more modern C++ features

I'm just a bunny disguised as a person, so forgive my bad code in advance.

You might also be interested in this other project of mine:

* Qt6 project using QtQuick + model code from C++, using C++ modules: https://codeberg.org/herzenschein/qt6-with-modules

## Building

This project requires clang++ 16+ and Ninja 1.11+ to be built,
as well as CMake 3.28+. This project is also planned to use C++20 and C++23
exclusive features like Ranges, Print, Concepts, and Expected.

To build, do:

```bash
cmake --preset default
cmake --build --preset default --parallel
cmake --install build/ --prefix ~/.local
```

The binary will be installed to `~/.local/bin/learning` and be available in
your $PATH by default.

If you want to use GCC or MSVC, you'll need to configure the project yourself.

CMake 3.28 is required to avoid the need for `CMAKE_CXX_SCAN_FOR_MODULES=ON`.

There is an additional preset that builds only the minimal C++20 and C++23
feature examples using `BUILD_MINIMAL_FEATURES=ON`:

```bash
cmake --build --preset minimal-features
```

When using libstdc++ (GNU's C++ implementation), there is a bug (or I guess a design decision) that doesn't let you use std::expected with clang yet: https://github.com/llvm/llvm-project/issues/62801. To work around this, I had to do an icky built-in macro redefinition of `-D__cpp_concepts=202002`. This makes the std::expected available as part of the `minimal-features` target.

The alternative would have been to create a whole separate configuration and build preset just for expected to use libc++, which is exactly what I did before.

This project uses a mix of cpp (module implementation unit) files and ixx (module interface units) files. If you don't like the implementation + header model and want to instead use a purely single file implementation model (with cppm, importable module units), see the `example/single-file-modules` branch.

## Known modules limitations

1. Not possible to use legacy header units.

This refers to doing things like:

```
module;
import <iostream>;
```

Supported by MSVC and experimental Clang but not CMake: https://gitlab.kitware.com/cmake/cmake/-/issues/25293

2. Not possible to use with the Unix Makefiles generator.

It's not supported or even planned for use with CMake.

You can technically write Makefiles with raw compiler instructions and the
necessary flags to build modules ~~if you're a masochist~~.

3. It is possible to use `import std;`, but only on libc++

Clang supports `import std;` since version 18.1.1, which means it's possible to use libc++ for it.

GCC will only get `import std;` in [version 15](https://gcc.gnu.org/gcc-15/changes.html), which means it's not possible to use libstdc++ for it just yet. To use the GCC 15 support in CMake, you'll also need [this MR](https://gitlab.kitware.com/cmake/cmake/-/merge_requests/10023), which only comes with CMake 4.0. I've compiled GCC and CMake myself and confirmed it works well.

The current project uses SFML built with libstdc++ on my machine (openSUSE Tumbleweed), and I'm not in the mood to build SFML just for this.

Requirements to use `import std;` with Clang:

* `cmake_minimum_required(VERSION 3.29)`
* `set(CMAKE_CXX_FLAGS -stdlib=libc++)`
* `set(CMAKE_EXPERIMENTAL_CXX_IMPORT_STD "0e5b6991-d74f-4b3d-a41c-cf096e0b2508")`
* `set(CMAKE_CXX_MODULE_STD ON)`
* the above should precede the `project()` call
* use `cxx_std_23` in `target_compile_features()`

Requirements to use `import std;` with GCC 15 and CMake 4.0:

* `cmake_minimum_required(VERSION 4.0)`
* `set(CMAKE_EXPERIMENTAL_CXX_IMPORT_STD "a9e1cf81-9932-4810-974b-6eccaf14e457")`
* `set(CMAKE_CXX_MODULE_STD ON)`
* the above should precede the `project()` call
* use `cxx_std_23` in `target_compile_features()`

## Good Modules Docs

* [The advantages of modules](https://www.modernescpp.com/index.php/cpp20-modules/)
* [Explaining C++ modules](https://blog.ecosta.dev/en/tech/explaining-cpp20-modules)
* [Microsoft Named modules tutorial](https://learn.microsoft.com/en-us/cpp/cpp/tutorial-named-modules-cpp)
* [C++20 modules update (useful comments)](https://discourse.cmake.org/t/c-20-modules-update/7330)
* [What is the file extension naming convention for c++20 modules?](https://stackoverflow.com/questions/75733706/what-is-the-file-extension-naming-convention-for-c20-modules)
